import {Pixelify_Sans, VT323} from "next/font/google"

export const pixelify = Pixelify_Sans({
	subsets: ["latin"],
	weight: ["400", "500", "600", "700"]
})

export const vt323 = VT323({
	subsets: ["latin"],
	weight: ["400"]
})